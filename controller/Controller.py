#!/usr/bin/env python3

import sys

from model.I_ToDo import I_ToDo
from view.I_View import I_View


class Controller:
    _model = None
    _view = None

    def __init__(self, model, view):
        if view == None:
            raise Exception("View cannot be empty")
        if model == None:
            raise Exception("Model cannot be empty")

        if not isinstance(model, I_ToDo):
            raise Exception("Model must implement I_ToDo (model = {0})".format(type(model)))

        if not isinstance(view, I_View):
            raise Exception("View must implement I_view (view = {0})".format(type(view)))

        self._model = model
        self._view = view

    def startApp(self):
        self._view.startView(self._model)


if __name__ == "__main__":
    version = ".".join(map(str, sys.version_info[:3]))
    print("Running on python version: {0}".format(version))
    print("run on Controller")
