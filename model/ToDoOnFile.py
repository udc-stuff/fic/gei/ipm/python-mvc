#!/usr/bin/env python3

import sys
from datetime import datetime
from pathlib import Path

from model.I_ToDo import I_ToDo


class ToDoOnFile(I_ToDo):
    _file = None
    _last_id = None

    def __init__(self, file):
        self._file = file

        f = Path(self._file)
        if not (f.is_file()):
            self._last_id = 1
            return

        # get the last_id from a file
        f = open(self._file, "r")
        lines = f.readlines()
        f.close()

        self._last_id = 0

        for line in lines:
            row = line.strip("\n").split(",")
            if int(row[0]) > self._last_id:
                self._last_id = int(row[0])

        self._last_id = self._last_id + 1

    def getTasks(self):
        f = open(self._file, "r")
        data = []
        for line in f:
            # print(line.strip("\n"))
            row = line.strip("\n").split(",")
            d = datetime.strptime(row[2], '%Y-%m-%d').date()

            data.append((int(row[0]), row[1], d, bool(row[3])))

        f.close()

        return data

    def addTask(self, task, date, done):
        index = self._last_id;
        self._last_id = self._last_id + 1

        f = open(self._file, "a+")
        f.write("{0},{1},{2},{3}\n".format(index, task, date, done))
        f.close()

        return index

    def editTask(self, id, task, date, done):
        f = open(self._file, "r")
        lines = f.readlines()
        f.close()

        f = open(self._file, "w")
        for line in lines:
            row = line.strip("\n").split(",")
            if int(row[0]) != id:
                f.write(line)
            else:
                f.write("{0},{1},{2},{3}\n".format(id, task, date, done))

        f.close()

    def deleteTask(self, id):
        f = open(self._file, "r")
        lines = f.readlines()
        f.close()

        f = open(self._file, "w")
        for line in lines:
            row = line.strip("\n").split(",")
            if int(row[0]) != id:
                f.write(line)

        f.close()



if __name__ == "__main__":
    version = ".".join(map(str, sys.version_info[:3]))
    print("Running on python version: {0}".format(version))
    print("run on ToDoOnFile")
