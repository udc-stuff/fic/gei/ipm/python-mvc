#!/usr/bin/env python3

import sys

from model.I_ToDo import I_ToDo


class ToDoOnMemory(I_ToDo):
    _data = None
    _last_id = None

    def __init__(self):
        self._data = []
        self._last_id = 1;

    def getTasks(self):
        # for item in self._data:
        #     print(item)

        return self._data

    def addTask(self, task, date, done):
        index = self._last_id;
        self._last_id = self._last_id + 1
        self._data.append((index, task, date, done))
        return index

    def editTask(self, id, task, date, done):
        item = None
        for index, item in enumerate(self._data):
            if item[0] == id:
                self._data[index] = (id, task, date, done)

    def deleteTask(self, id):
        self._data = [item for item in self._data if item[0] != id]


if __name__ == "__main__":
    version = ".".join(map(str, sys.version_info[:3]))
    print("Running on python version: {0}".format(version))
    print("run on ToDoOnMemory")