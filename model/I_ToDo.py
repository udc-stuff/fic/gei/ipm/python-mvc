#!/usr/bin/env python3

import sys

import abc


class I_ToDo(abc.ABC):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def getTasks(self):
        pass

    @abc.abstractmethod
    def addTask(self, task, date, done):
        pass

    @abc.abstractmethod
    def editTask(self, id, task, date, done):
        pass

    @abc.abstractmethod
    def deleteTask(self, id):
        pass


if __name__ == "__main__":
    version = ".".join(map(str, sys.version_info[:3]))
    print("Running on python version: {0}".format(version))
    print("run on I_ToDo")