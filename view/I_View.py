#!/usr/bin/env python3

import sys

import abc


class I_View(abc.ABC):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def startView(self, model):
        pass


if __name__ == "__main__":
    version = ".".join(map(str, sys.version_info[:3]))
    print("Running on python version: {0}".format(version))
    print("run on I_View")