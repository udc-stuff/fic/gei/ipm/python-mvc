#!/usr/bin/env python3

import sys

from datetime import datetime

from model.I_ToDo import I_ToDo

from view.I_View import I_View


class V_CommandLine(I_View):
    _model = None

    def __init__(self):
        pass

    def startView(self, model):
        if not isinstance(model, I_ToDo):
            raise Exception("Model must implement I_ToDo (model = {0})".format(type(model)))

        self._model = model

        while True:
            c = self._showMenu()

            if c == "1":
                self._showTasks()
            elif c == "2":
                self._addTask()
            elif c == "3":
                self._editTask()
            elif c == "4":
                self._deleteTask()
            elif c == "5":
                break
            else:
                print("Invalid option")

    def _showMenu(self):
        print("Welcome to new ToDo App!!!")
        print("")
        print("1) Show tasks")
        print("2) Add new task")
        print("3) Edit a task")
        print("4) Delete task")
        print("")
        print("5) Exit")
        print("")
        print("Choose an option:")

        return sys.stdin.readline().strip()

    def _showTasks(self):
        tasks = self._model.getTasks()

        if len(tasks) == 0:
            print("No tasks")
            return

        for item in tasks:
            print(item)

    def _getTaskInfo(self):
        # get the task name
        task = ""
        while task == "":
            print("Write the task:")
            task = sys.stdin.readline().strip()

        # Get the deadline
        deadline = ""
        while deadline == "":
            print("Write the deadline (yyyy/mm/dd):")
            deadline = sys.stdin.readline().strip()

            try:
                date = datetime.strptime(deadline, '%Y/%m/%d')
            except ValueError:
                deadline = ""

        # Get if the task is done
        done = ""
        while done != "true" and done != "false":
            print("It's done? (True|False):")
            done = sys.stdin.readline().strip().lower()

        if done == "true":
            done = True
        else:
            done = False

        return (task, date, done)

    def _addTask(self):
        tmp = self._getTaskInfo()

        self._model.addTask(tmp[0], tmp[1], tmp[2])

    def _editTask(self):
        line = ""
        while line == "":
            print("Task id to edit:")
            line = sys.stdin.readline().strip()
            try:
                id = int(line)
                break
            except ValueError:
                line = ""

        tmp = self._getTaskInfo()

        self._model.editTask(id, tmp[0], tmp[1], tmp[2])


    def _deleteTask(self):
        line = ""
        while line == "":
            print("Task id to delete:")
            line = sys.stdin.readline().strip()
            try:
                id = int(line)
                break
            except ValueError:
                line = ""

        self._model.deleteTask(id)

if __name__ == "__main__":
    version = ".".join(map(str, sys.version_info[:3]))
    print("Running on python version: {0}".format(version))
    print("run on V_CommandLine")