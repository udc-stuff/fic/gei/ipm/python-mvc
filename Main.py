#!/usr/bin/env python3

import sys
from datetime import date

from controller.Controller import Controller

from model.I_ToDo import I_ToDo
from model.ToDoOnFile import ToDoOnFile
from model.ToDoOnMemory import ToDoOnMemory

from view.I_View import I_View
from view.commandLine.V_CommandLine import V_CommandLine


def _checkModel():
    toDo = ToDoOnMemory()
    # toDo = ToDoOnFile('/tmp/task.csv')

    print("toDo is instance of I_ToDo: {0}".format(isinstance(toDo, I_ToDo)))
    print("toDo is instance of ToDoOnMemory: {0}".format(isinstance(toDo, ToDoOnMemory)))
    print("toDo is instance of ToDoOnFile: {0}".format(isinstance(toDo, ToDoOnFile)))

    toDo.addTask("do homework", date(2017, 8, 1), False)
    toDo.editTask(1, "do homework", date.today(), True)

    toDo.addTask("go to the cinema", date(2018, 8, 2), False)
    toDo.deleteTask(2)

    toDo.getTasks()

def _checkView():
    view = V_CommandLine()

    print("view is instance of I_View: {0}".format(isinstance(view, I_View)))
    print("view is instance of V_CommandLine: {0}".format(isinstance(view, V_CommandLine)))

if __name__ == "__main__":
    version = ".".join(map(str, sys.version_info[:3]))
    print("Running on python version: {0}".format(version))

    controller = Controller(ToDoOnMemory(), V_CommandLine())
    controller.startApp()

    print("closing app...")